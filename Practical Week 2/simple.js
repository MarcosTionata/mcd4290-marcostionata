//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = ""
    //Question 1 here
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73,     19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    let positiveOdd = [];
    let negativeEven = [] ;
    for(let i = 0; i <= myArray.length; i++){
        if(myArray[i] > 0 && myArray[i] % 2 !== 0){
          positiveOdd.push(myArray[i])  
        }
        else if(myArray[i] < 0 && myArray[i] % 2 ===0){
          negativeEven.push(myArray[i])
        }  
    }
    output += "Postive odd : " + positiveOdd + "\n"
    output += "Negative even: " + negativeEven + "\n"
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = ""
    //Question 2 here
    var FreqValue1 = 0;
    var FreqValue2 = 0;
    var FreqValue3 = 0;
    var FreqValue4 = 0;
    var FreqValue5 = 0;
    var FreqValue6 = 0;
    for (i = 0; i < 60000; i++){
        randNum = Math.floor((Math.random() *6)+ 1);
        if(randNum === 1){
            FreqValue1++;
        }
        else if (randNum === 2){
            FreqValue2++;
        }
        else if (randNum === 3){
            FreqValue3++;
        }
        else if (randNum === 4){
            FreqValue4++;
        }
        else if (randNum === 5){
            FreqValue5++;
        }
        else if (randNum === 6){
            FreqValue6++;
        }
        
    }
    
    output += "Frequencies of die rolls"+ "\n"
    output += "1: " + FreqValue1 + "\n"
    output += "2: " + FreqValue2 + "\n"
    output += "3: " + FreqValue3 + "\n"
    output += "4: " + FreqValue4 + "\n"
    output += "5: " + FreqValue5 + "\n"
    output += "6: " + FreqValue6 + "\n"
    
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    let diceArray = [0, 0, 0, 0, 0, 0, 0];
    for(i = 0; i < 60000;i++){
        diceArray[Math.floor((Math.random() *6)+ 1)]++;
    }
    output += "Frequency of die rolls "+ "\n";
    for(var index = 1; index <= 6; index++){
       output += index + ": " + diceArray[index] + "\n"
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here
    var Dicerolls = {
        frequencies: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
        },
        total: 60000,
        exceptions: ""
    };
    for (var roll = 0; roll < Dicerolls.total; roll++) {
        var randVal = Math.floor((Math.random() *6)+ 1);
        Dicerolls.frequencies[randVal]++;
    }
    var expValue = 10000
    var onepercent = expValue * 0.01;
    for (var index = 1; index<= 6; index++) {
        var difference = Dicerolls.frequencies[index] - expValue;
        if (Math.abs(difference) > onepercent){
            Dicerolls.exceptions += index + " ";
        }
    }
    //Output of the result (Properties of the object)
    output += "Frequency of die rolls " + "\n";
    output += "Total rolls : " + Dicerolls.total + "\n";
    output += "Frequencies:"+ "\n";
    for (var index in Dicerolls.frequencies) {
        output += index + ": " + Dicerolls.frequencies[index] +"\n";
    }
    output += "Exceptions: " + Dicerolls.exceptions +"\n";
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = {
        Name : "Jane",
        Income : 127050
    }
    let Janeincome = person.Income;
    let tax;
    if (Janeincome < 18200){
        tax = 0;
    } else if (Janeincome >= 18201 && Janeincome <= 37000) {
        tax = 0.19 * (Janeincome - 18200)
    } else if (Janeincome >= 37001 && Janeincome <= 90000) {
        tax = 3572 + (0.325 * (Janeincome - 37000))
    } else if (Janeincome >= 90001 && Janeincome <= 180000) {
        tax = 20797 + (0.37 * (Janeincome - 90000))
    } else if(Janeincome >= 180001){
        tax = 54097 + 0.45 * (Janeincome - 180000)
    }
    output += person.Name + "'s income is : $" + Janeincome + ", and her tax owed is: $" + tax
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}