//to check the code, uncomment the corresponding question.

question1();
question2();
//question3();
question4();
//question5();

function question1() {
    let output = "" //empty output, fill this so that it can print onto the page.

    //Question 1 here 
    function ObjectToHTML(myObj) {
        for (let i in myObj) {
            output += i + ": " + myObj[i] + " ";
        }

    }
    var testObj = {
        number: 1,
        string: 'abc',
        array: [5, 4, 3, 2, 1],
        boolean: true
    }

    ObjectToHTML(testObj)

    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2() {
    let output = ""

    //Question 2 here 
    function flexible(fOperation, operand1, operand2) {
        var result = fOperation(operand1, operand2)
        return result;
    }

    function addition(num1, num2) {
        return num1 + num2
    }

    function multiplication(num1, num2) {
        return num1 * num2
    }

    output += flexible(addition, 3, 5) + " ";
    output += flexible(multiplication, 3, 5);

    let outPutArea = document.getElementById("outputArea2")
    outPutArea.innerText = output
}

/*function question3(){
    let output = "" 
    //Question 3 here 
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}
//Task 3
//First of all define the function extremevalues by stating it's parameter which is an array. Next is to fill the body part of the function with the condition that you want. So, here the condition is to find the minimum value and maximum value of the array. To start with, give a variable name minNumber and max number soo that the value of minimum and maximum number will be stored there. Assume the first index in the array is the max then use for loop to compare if the first index or the second index is bigger or if it's not it'll go to the loop and see the second and third index. Make another for loop with the same step and find the min number
*/

function question4() {
    let output = ""

    //Question 4 here
    function extremevalues(myArray) {
        let minimum = myArray[0];
        let maximum = myArray[0];

        for (i = 0; i < myArray.length; i++) {
            if (myArray[i] < minimum) {
                minimum = myArray[i];
            }
        }
        for (j = 0; j < myArray.length; j++) {
            if (myArray[j] > maximum) {
                maximum = myArray[j];
            }
        }
        output = maximum + " " + minimum;
    }

    let values = [4, 3, 6, 12, 1, 3, 8];

    extremevalues(values)
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output
}

/*function question5(){
    let output = "" 
    
    //Question 5 here 
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}*/